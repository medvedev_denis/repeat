package ru.dm.nu10;

import java.util.Scanner;

public class NumPalindrom {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число");
        String numString = scanner.nextLine();
        while (!isInt(numString)) {
            System.out.println("Некорректное число. Пожалуйста введите число заново");
            numString = scanner.nextLine();
        }
        int j = 0;
        if (numString.length() % 2 == 0) {
            for (int i = 0; i <= numString.length() / 2; i++) {
                if (numString.charAt(i) == numString.charAt(numString.length() - 1 - i)) {
                } else {
                    j = 1;
                    break;
                }
            }
        } else {
            j = 1;
        }
        if (j == 1) {
            System.out.println("Введенное число не является полиндромом");
        } else {
            System.out.println("Введенное число является полиндромом");
        }
    }

    public static boolean isInt(String x) throws NumberFormatException {
        try {
            Integer.valueOf(x);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}