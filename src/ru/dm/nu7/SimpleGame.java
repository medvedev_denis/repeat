package ru.dm.nu7;
import java.util.Random;
import java.util.Scanner;

public class SimpleGame {
    static Scanner scanner = new Scanner(System.in);
    static final Random random = new Random();
    public static int hidNum;
    public static int insertNum;
    public static void main(String[] args) {
        hidNum = random.nextInt(100)+1 ;
        System.out.println("Попытайтесь угадать случайное число от 1 до 100");
        int i = 0;
        do {
            insertNum=scanner.nextInt();
            if (insertNum < hidNum) {
                System.out.println("Заданное число больше");
            } else {
                if (insertNum > hidNum) {
                    System.out.println("Заданное число меньше");
                } else {
                    System.out.println("Вы угадали загаданное число. Загаданное число = " + hidNum);
                    System.out.println("Попыток затрачено до правильно найденого числа" + i);
                }
            }
            i++;
        } while (hidNum!=insertNum);
    }
}
