package ru.dm.nu9;

import java.util.Random;
import java.util.Scanner;

public class NullColumn {
    static Random random = new Random();
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        int line = random.nextInt(5) + 1;
        int column = random.nextInt(5) + 1;
        int[][] matr = new int[line][column];
        for (int i = 0; i <line; i++) {
            for (int j = 0; j <column; j++) {
                matr[i][j] = random.nextInt(1000) + 1;
            }
        }
        System.out.println("Введите число <= " + column);
        String numString = scanner.nextLine();
        while (!isInt(numString, column)) {
            System.out.println("Некорректное число. Пожалуйста введите число заново");
            numString = scanner.nextLine();
        }
        int userNum = Integer.parseInt(numString);
        System.out.println("Исходная матрица:");
        matrPrint(matr, line, column);
        for (int i = 0;i<line;i++){
            matr[i][userNum - 1] = 0;
        }
        System.out.println("Полученая матрица:");
        matrPrint(matr, line, column);
    }

    public static void matrPrint(int[][]matr, int column, int line){
        for (int i = 0; i<column; i++){
            for (int j = 0; j<line; j++){
                System.out.print(matr[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static boolean isInt(String x, int column) throws NumberFormatException {
        try {
            if (Integer.parseInt(x)>column || Integer.parseInt(x)<= 0){
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }
}
