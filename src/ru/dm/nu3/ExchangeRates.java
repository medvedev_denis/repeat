package ru.dm.nu3;
import java.util.Scanner;

public class ExchangeRates {
    static Scanner scanner = new Scanner(System.in);
    public static double countRub;
    public static double course;
    public static double countEur;
    public static void main(String[] args) {
        System.out.println("Введите курс валют");
        course=scanner.nextDouble();
        System.out.println("Введите сколько рублей вы хотите перевести");
        countRub=scanner.nextDouble();
        countEur=convert(countRub,course);
        System.out.println(countEur);
    }

    private static double convert(double countRub,double course){
        return countRub/course;
    }
}
