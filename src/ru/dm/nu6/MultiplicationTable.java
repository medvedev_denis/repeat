package ru.dm.nu6;
import java.util.Scanner;
public class MultiplicationTable {
    static Scanner scanner = new Scanner(System.in);
    public static double userNum;
    public static void main(String[] args) {
        System.out.println("Введите число");
        userNum=scanner.nextDouble();
        for (int i=1;i<10;i++){
            System.out.println(userNum + " * " + i + " = " + userNum*i);
        }
    }
}
