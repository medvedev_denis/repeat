package ru.dm.nu13;


import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class MassTransfer {
    static Random random = new Random();
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        int line = random.nextInt(5) + 1;
        int column = random.nextInt(5) + 1;
        int line1 = line*column;
        int[][] matr = new int[line][column];
        int[] mass = new int[line1];
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                matr[i][j] = random.nextInt(1000) + 1;
            }
        }
        int arrayCell = 0;
        for (int i = 0; i <line; i++) {
            for (int j = 0; j <column; j++) {
                mass[arrayCell] = matr[i][j];
                arrayCell = arrayCell + 1;
            }
        }
        System.out.println("Начальная матрица:");
        for (int i = 0; i<line; i++){
            for (int j = 0; j<column; j++){
                System.out.print(matr[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("Полученый массив");
        System.out.println(Arrays.toString(mass));
    }
}
