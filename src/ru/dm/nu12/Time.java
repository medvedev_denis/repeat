package ru.dm.nu12;

import java.util.Scanner;

public class Time {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите количество суток");
        int days=scanner.nextInt();
        int hour=days*24;
        int minute=hour*60;
        int second=minute*60;
        System.out.print("Часов: " + hour + ",\nминут: " + minute + ",\nсекунд: " + second + '.');
    }
}
