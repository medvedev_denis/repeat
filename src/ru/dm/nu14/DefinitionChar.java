package ru.dm.nu14;

import java.util.Scanner;

public class DefinitionChar {
    static Scanner scanner = new Scanner(System.in);
    static char usedChar;
    public static void main(String[] args) {
        System.out.println("Введите 1 символ:");
        String userString = scanner.nextLine();
        useChar(userString);
        boolean character = true;
        if (Character.isDigit(usedChar)){
            System.out.println("Введенный символ является цифрой!");
            character = false;
        }
        if (Character.isLetter(usedChar)){
            System.out.println("Введенный символ является буквой!");
            character = false;
        }
        String usedString = "" +  usedChar;
        if(". , : ; ? !".contains(usedString)){
            System.out.println("Введенный символ является знаком пунктуации!");
            character = false;
        }
        if(character){
            System.out.println("Введенный символ не является цифрой, буквой или знаком пунктуации!");
        }
    }

    public static void useChar(String userString)throws ArrayIndexOutOfBoundsException{
        char[] userChar = new char[1];
        try {
            for (int i = 0; i<userString.length(); i++) {
                userChar[i] = userString.charAt(i);
            }
            System.out.println("Введенный символ принят!");
            usedChar = userChar[0];
        } catch (ArrayIndexOutOfBoundsException e){
            usedChar = userString.charAt(0);
            System.out.println("Обнаружена последовательность символов. Для проверки будет взят первый символ!");
        }

    }
}
