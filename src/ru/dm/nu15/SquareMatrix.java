package ru.dm.nu15;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class SquareMatrix {
    static Scanner scanner = new Scanner(System.in);
    static final Random random = new Random();
    public static void main(String[] args) {
        System.out.println("Введите размер квадратной матрицы");
        int n = scanner.nextInt();
        int[][] matr = new int[n][n];
        int[][]transposeMatr = new int[n][n];
        for (int i = 0; i<n; i++){
            for (int j = 0;j<n; j++){
                matr[i][j] = random.nextInt(1000)+1;
                transposeMatr[j][i] = matr[i][j];
            }
        }
        System.out.println("Исходная матрица:");
        matrPrint(matr,n);
        System.out.println("Транспонированная матрица:");
        matrPrint(transposeMatr,n);
    }

    public static void matrPrint(int[][]matr, int n){
        for (int i = 0; i<n; i++){
            for (int j = 0; j<n; j++){
                System.out.print(matr[i][j] + " ");
            }
            System.out.println();
        }
    }
}
