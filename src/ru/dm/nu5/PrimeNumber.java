package ru.dm.nu5;

public class PrimeNumber {
    public static void main(String[] args) {
        int primeNumber = 2;
        System.out.println("Простые числа: \n" + primeNumber);
        for (int i = 3; i<=100; i++){
            if (isPrimeNum(i)){
                primeNumber = i;
            } else {
                continue;
            }
            System.out.println(primeNumber);
        }
    }

    public static boolean isPrimeNum(int num){
        for (int i=2; i<=Math.sqrt(num) ;i++){
            double divNum = num/2.0;
            double sqrtNum = Math.sqrt(num);
            if (num/i == divNum || num/i == sqrtNum){
                return false;
            } else {
                continue;
            }
        }
        return true;
    }
}
