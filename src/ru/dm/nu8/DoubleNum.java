package ru.dm.nu8;

import java.util.Scanner;

public class DoubleNum {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число");
        double testDoubleNum = scanner.nextDouble();
        int testIntNum = (int) testDoubleNum;
        if (testDoubleNum == testIntNum) {
            System.out.println("Введенное число является целым");
        } else {
            System.out.println("Введенное число не является целым");
        }
    }
}
